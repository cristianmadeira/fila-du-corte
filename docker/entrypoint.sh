#!/bin/bash

npm install
echo 'Installing js package....'
npm run dev

php artisan migrate

php artisan db:seed

php artisan serve --host=0.0.0.0
