@extends('adminlte::page')
@section('title','Profile')

@section('content')

        <form>
            <div class="row">
                <x-adminlte-input
                    name="email"
                    label="E-mail"
                    placeholder="placeholder"
                    fgroup-class="col-md-6"
                    disable-feedback
                    value="{{$user->email}}"/>

                <x-adminlte-input
                    name="username"
                    label="Username"
                    placeholder="placeholder"
                    fgroup-class="col-md-6"
                    disable-feedback
                    value="{{$user->name}}"/>
            </div>
            <div class="row">
                <x-adminlte-input
                    name="phone"
                    label="Phone"
                    placeholder="Input your phone"
                    fgroup-class="col-md-6"
                    value="{{$user->phone}}" />
            </div>

                <x-adminlte-button
                    theme="primary"
                    label="Update"
                    type="submit"/>


        </form>

@stop
